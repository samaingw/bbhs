# Branch and Bound - Unmixing problem in Hyperspectral imaging.
### Conception d'un algorithme de type Branch and Bound pour le problème de démélange en imagerie hyperspectrale.  

---

Stage Master 2 - Optimisation en recherche opérationnelle - Université de Nantes.  

**Stagiaire :**   
  * [Mehdi Latif](http://mlatif.fr)  [@](mailto:latifm.pro@gmail.com).

**Encadrant(s) :**
  * [Sébastien Bourguignon](http://pagesperso.ls2n.fr/~bourguignon-s/).  
  * Ramzi BEN MHENNI [@](mailto:ramzi.benmhenni@ls2n.fr).  

**Langages :** Matlab, C++.

---
### Problématique :
