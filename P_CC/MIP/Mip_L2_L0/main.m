clear all   % 
close all   %
windows = 0; % Juste un booleen pour selectionner l'OS
linux = 0;
mac = 1;
global verbose;
verbose = 0; % booleen pour l'affichage verbeux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% path cplex install
if(windows)
    addpath('C:\Program Files\IBM\ILOG\CPLEX_Studio126\cplex\matlab\x64_win64');
    addpath('C:\Program Files\IBM\ILOG\CPLEX_Studio126\cplex\examples\src\matlab');
elseif (linux)
    addpath('/opt/ibm/ILOG/CPLEX_Studio126/cplex/matlab');
    addpath('/opt/ibm/ILOG/CPLEX_Studio126/cplex/examples/src/matlab');
elseif(mac)
    addpath(genpath('/Users/mehdilatif/opt/ibm/ILOG/CPLEX_Studio1210/cplex/matlab')); 
    addpath(genpath('/Users/mehdilatif/opt/ibm/ILOG/CPLEX_Studio1210/cplex/examples/src/matlab'));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Appel aux fonctions du B&B 
addpath('./B&B/');
%addpath('/B&B/relaxation');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D�claration de variables globales pour contenir les dictionnaires (H,HH)
global H;       
% D�claration des param�tres exp�rimentaux 
%   - K : coefficient de parcimonie
%   - SNR : Signal-to-noise ratio (un indicateur de la qualit� de la
%   transmission d'une information)
%   - instance : n� de l'instance consid�r�e
% => Permet l'appel de l'instance du probl�me consid�r�
K=5;
SNR=10;
instance=1;
% Ramzi 
%filename=strcat(['../../multimedia_material_sectionV/SA_SNR',num2str(SNR),'_K',num2str(K),'_instance',num2str(instance),'.mat']);
% Mehdi
filename=strcat(['../../multimedia_material_sectionV/mehdi_SA_SNR',num2str(SNR),'_K',num2str(K),'_instance',num2str(instance),'.mat']);
% Mini instance 
filename="../../multimedia_material_sectionV/miniInstance.mat";

load(filename);
verbose=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
 %TODO D�commenter dans un cas autre que la miniinstance
% Chargement du dictionnaire - Calcul de la matrice H
% load('../H.dat');
% HH=H'*H;



% % R�cup�ration des dimension de H (nb Endmembers, nb Obs)
[N,Q]=size(H);  
% Initialisation de la solution de base (
x0=zeros(Q,1);
% Calcul du bigM pour la relaxation (#TODO fixer � 1 dans notre probl�me)
M = 1.1*max(abs(H'*y))/norm(H(:,1))^2;
% Cr�ation de la structure du mod�le
options = cplexoptimset;
options.relaxation=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Appel au Branch and bound 
[x_BB_QP,M_BB_QP,exitflag_BB_QP,output_BB_QP,nodes_cplex]=optiMIP_minL2_constL0(y,H,K,x0,M,options,[],verbose);
% 
data.M=M;
data.y=y;
data.K=K;
data.SNR=SNR;
data.x_Truth=x_Truth;
% 
% Affichage des r�sultats en sortie
% reportname = strcat(['syntBB_SA_SNR',num2str(SNR),'_K',num2str(K),'_instance',num2str(instance),'.txt']);
% fid = fopen(reportname,'at');
%     fprintf("TOTO\n");
% fclose(fid);
%figure; hold on ; stem(output_BB_QP.x,'b+') ; plot(-data.M*ones(100,1)); plot(data.M*ones(100,1));
% 




%save(['../../multimedia_material_sectionV/mehdi_SA_SNR',num2str(SNR),'_K',num2str(K),'_instance',num2str(instance),'.mat'],'data','H','HH','K','SNR','instance','verbose','x0','x_Truth','y');
