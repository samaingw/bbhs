function [x,M,exitflag,output,c]  = optiMIP_minL2_constL0(y,H,K,x0,M,options,ordVarSel,verbose)
%{
optiMIP_minL2_constL0
    Input : 
        - y : donn�es observ�es
        - H : le dictionnaire
        - K : le coefficient de parcimonie
        - x0 : la solution initiale
        - options : le solveur
        - ordVarSel :  vecteur Strat�gie de choix des variables (Voir dans ./B&B/Selecting_Variables.m)
        - verbose : ajout pour contr�ler le mode verbose dans la sous
        fonctions
    Output : 
        - x : La composition du m�lange.
        - M : Valeur du bigM en fin d'execution %TODO �a � l'air d'�tre le
        bigM non modifi�.
        - exitflag : potentielle erreur rencontr�e apr�s execution du Hde MIP_BNB 
        - output : Objet contenant les �l�ments de solution apr�s excution
        du BB {x,resnorm, temps_BB (temps d'ex�cution),c}
        - c : Nombres de noeuds parcourus par le branch and bound
%}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	options.MaxTime = 1000;
    options.Display='on';
    [N,Q]=size(H); 

    if(Q<=N)
        % Cr�ait une sparse matrix de taille NxQ vide (ie remplie de zeros) : Z_NQ 
        Z_NQ = spdiags(zeros(N,1),0,N,Q);
        % Permet de r�cup�rer les �l�ments non nuls de H et les stocke dans C
        % sous forme {(x,y)     val} 
        C =  [H, Z_NQ];
        % Cr�ait une matrice identit�e sparse ie (i,i) = 1 \forall i \in 1:|Q|
        I_Q = speye(Q);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Cr�ation du vecteur colonne "membre droit" des contraintes in�galit�s du probl�mes pour la mod�lisation avec cplex
        bineq = zeros(2*Q,1);    %2*Q ?? %TODO Question � poser ou � �claircir 
        bineq = [bineq;K];        %Concat�nation de la ctr de parcimonie en btm du vecteurs d'ineq
        % Vecteurs des bornes inf�rieures et sup�rieures 
        lb = [-M*ones(Q,1);zeros(Q,1)];
        ub = [+M*ones(Q,1);ones(Q,1)];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Cr�ation du vecteur colonne "membre droit" et de la matrice des contraintes �galit�s du probl�mes pour la mod�lisation avec cplex
        Aeq = [];
        beq = [];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % xtype(1:Q) = 0; xtype(Q+1:2*Q)=1; xtype = xtype';
        xtype=[zeros(Q,1);ones(Q,1)]; 
        %     
        z0(1:Q,1) =  x0;
        z0(Q+1:2*Q,1) = (x0~=0);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Boolean : permet de stoper l'ex�cution du BB apr�s une premi�re
        % it�ration.
        M_OK = 0;
        while ~M_OK 
            Aineq = [I_Q,    -M*I_Q;
                    -I_Q,   -M*I_Q];
            Aineq = [Aineq;
                    zeros(1,Q), ones(1,Q)];
            % A_f_in  = full(Aineq) % A est sparse donc pour obtenir la
            % vraie forme de A, utliser full(.)
            %TQ%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
            % Param�trage des �l�ments de r�solution
            options.Display='off';
            % Voir fonctions  ./B&B/Selecting_Branch.m et  ./B&B/Selecting_Variables.m
            heuristic.choice_Selection = 1;     % Strat�gie de choix de la variable � brancher
            heuristic.choice_Separation = 1;    % Choix des strat�gies de s�paration 
            heuristic.ordVarSel=ordVarSel;      % Ordre impos� dans le choix des variable (utilis� dans ./B&B/Selecting_Variables.m)
            heuristic.nodeStrategy = 'bn';
            options.heuristic=heuristic;
            % Cr�ation de l'objet param�tre (permet de passer d'un seul coup tous les param�tres d�finis plus haut                 
            params.z0=z0;
            params.xtype=xtype;
            params.K=K;
            params.M=M;
            % Chronom�trage
            temps_BB = 0;                   
            % options = le solveur(mod�le) avec les sp�cifit� pass�e en param�tre
            % params = les diff�rentes valeurs de constantes dont on a besoin pour construire le mod�le.
            
            [~,resnorm,zsol,~,c,exitflag] = MIP_BNB_L2_L0(C,y,params,lb,ub,Aineq,bineq,Aeq,beq,options,verbose);
            
           
            % Gestion du cas d'erreur : si le BB n'a pas trouv� de solution
            % apr�s l'exploration totale 
            if exitflag<0
                error(sprintf('optimization failed, exitflag = %g (probably no feasible solution)',exitflag));
            end
            % R�cup�ration de la solution
            x = zsol(1:Q);
    %       % Paquetage des r�sultats de obtenus par le BB.
            output.x=x;
            output.z=resnorm;
            output.time=temps_BB;
            output.node=c;
            % Boolean de s�curit� - Pass� � 1 apr�s une ex�cution du BB        
            M_OK =1;
        end
    end
%     if(verbose)
%         fprintf("output.x : dim(%d,%d)\n",size(output.x,1),size(output.x,2))
%         disp(output.x)
%         fprintf("output.z : dim(%d,%d)\n",size(output.z,1),size(output.z,2))
%         disp(output.z)
%         fprintf("temps_BB = %d (sec)\n",temps_BB)
%     end
end




