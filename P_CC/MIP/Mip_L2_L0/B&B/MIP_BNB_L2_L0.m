function [errMsgG,Z,X,t,c,exitflag] = MIP_BNB_L2_L0(H,y,params,xl,xu,a,b,aeq,beq,options,verbose)
%{
MIP_BNB_L2_L0
    Input : 
        - H (= C) contient les �l�ments de H (dictionaire) sous forme de sparse matrix
        - y (=y) donn�es d'observations
        - params (=param) block de param�tres (z0, xtype, K,M)
        - xl (=lb) lower bound sur x
        - xu (=ub) upper bound sur x
        - a (= Aineq)    - matrice des membres gauches des contraintes d'in�galit�s
        - b (= bineq)    - vecteur des membres droits des contraintes d'in�galit�s
        - aeq (= Aeq)  - matrice des membres gauches des contraintes d'�galit�s
        - beq (= beq)  - vecteur des membres droits des contraintes d'in�galit�s
        - options : le solveur {relaxation, MaxTime,Display,Heuristic {choice_selection,choice_separation ordVarSel,nodeStrategy}}
        - verbose: ajout pour contr�ler le mode verbose dans la sous fonctions
        -
    Output : 
        - errMsgG : 
        - Z : valeur de la meilleure solution trouv�e � la fin de l'exploration de l'arbre. 
        - X : vecteur solution correspondant � la meilleure composition
        trouv�e � la fin de l'exploration de l'arbre. 
        - t : temps de r�solution.
        - c : nombre de noeuds explor�s
        - exitflag
        -
        -
    Note : sont g�n�r�es � partir de la fonction input_checking : 
        - errMsgIC : erreurs rencontr�e dans la v�rification de l'int�grit� des donn�es (IC pour Input Checking)
        - stopFlag (bool�en) : true si tout est ok
        - Aineq : la matrice des membres gauches des contraintes d'in�galit�s
        - bineq : le vecteur des membres droits des contraintes in�galit�s
        - Aeq :  la matrice des membres gauches des contraintes d'�galit�s
        - Beq : le vecteur des membres droits des contraintes �galit�s
        - xLb : les lower bounds sur les valeurs des x
        - xUb : les upper bounds sur les valeurs des x
        - xstatus : Compos� de Q fois 0 puis Q fois 1 - Indicatrice de la nature de la variable (est un xi -> 0, est un bi -> 1, est une constante 2) 
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    global maxSQPiter;
    maxSQPiter=1000;
    exitflag=0;
    Z=[]; X=[]; t=0;
    errMsgG = "";   
    timemax=1000;   % Temps autoris� pour la r�slution du branch and bound
    errx=1e-5;      % Erreur autoris�e
    pourcent=0;     % Joli pourcentage de r�solution made in Ramzi 
    %%%%%%%%%%%%%%%%
    % c : nombre de noeuds parcours 
    % fail (conditions : separation==1 & isempty(V)) 
    c=0; fail=0;
    %%%%%%%%%%%%%%%%
    relaxation_type= options.relaxation;
    heuristic=options.heuristic;
    %%%%%%%%%%%%%%%%
    x0=params.z0;
    xtype=params.xtype;
    K = params.K;
    M=params.M;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % STEP 0 : V�rification de l'int�grit� des variables. dernier param�tre verbose = 0;
    [errMsgIC,stopFlag,xstatus,xLb,xUb,Aineq,bineq,Aeq,Beq] = input_Checking(x0,xtype,xl,xu,a,b,aeq,beq,0);
    
    %%%%%%%%%%%%%%%%
    % Mise en place d'une s�curit� suppl�mentaire (permettant de lancer l'exec du BB ssi toutes les entr�es sont correctes) 
    if(~stopFlag)
        %tic;   %Chronom�trage
        lx = size(x0,1);
        %test ="1";
        % Si le vecteur initial d�passe les bornes inf ou sup, ou le ram�ne � la borne : ainsi, 
        % les valeurs en-dessous de la borne inf (xLb) sont ramen�es � xLb, c'est de l'indexation 
        % vectorielle � la sauce Matlab.
        x0(x0<xLb)=xLb(x0<xLb);  
        x0(x0>xUb)=xUb(x0>xUb); 
        %%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % STEP 1.1 : Initialisation de la solution courante et de la valeur de la borne sup. 
        % Fixation de la borne sup au sommet de l'arbre.
        if(all(Aineq*x0<=bineq))
            %if(verbose),fprintf("> RootUB -> All Ax<= B\n"); end
            z_incumbent= 0.5*x0'*(H)'*H*x0 -y'*H*x0;
            x_incumbent=x0;
        else
            %if(verbose),fprintf("> RootUB -> General case\n");end
            z_incumbent=inf;
            % x_incumbent=inf*ones(lx); % %TODO Une erreur possible car ones(lx) retourne une matrice carr�e lx*lx
            x_incumbent=inf*ones(lx,1);
        end
        
        if(verbose),print_current_info(0,z_incumbent,z_incumbent,x_incumbent);end

    % STEP 1.2 : D�claration de la liste de noeuds en attente
        I = ceil(sum(log2(xUb(xstatus==1)-xLb(xstatus==1)+1))+size(find(xstatus==1),1)+1);
        I = K+1;
        %test="5";    
        stackxlb=zeros(lx,I);   % Matrice de lx = 2Q lignes par I = colonnes
        stackxlb(:,1)=xLb;      % Affectation des xLb valeurs (ie Qx -M puis Qx 0)
        %test="6";  
        stackxub=zeros(lx,I);   % M�me format que stackxlb
        stackxub(:,1)=xUb;      % Affectation des xUb valeurs (ie Qx +M puis Qx 1)
        stackdepth=zeros(1,I);  % vecteur ligne de taille 1 ligne I colonnes 
        stackdepth(1,1)=1;      % affectation de la premi�re colonnes => (1, 0 ... 0 (I-1 fois 0)) 
        stacksize=1;
        %test="8";
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % STEP 2 : D�but de l'algorithme de branch and bound      
        timerelaxation=0;   % initialisatio du chrono pour le parcours de l'arbre;
        while (stacksize > 0 && timerelaxation < timemax ) % Note : Ineg Stricte sur le temps (on �vite alors de relancer un exp alors qu'il reste 2sec) 
            c = c + 1;  % update du nombre de noeud explor� 
            %if(verbose),print_current_BB_stack(c,stackdepth,stacksize);end
       % STEP 2.1 : Choix dun noeud     
            % Principe : les diff�rentes bsup et binf (apr�s branchements) sont stock�es dans deux matrices. L'indice stacksize nous
            % permet d'acceder aux binf/bsup du probl�me que l'on souhaite r�soudre � un noeud de l'arbre donn� 
            xLb=stackxlb(:,stacksize);  
            xUb=stackxub(:,stacksize);  
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            depth=stackdepth(1,stacksize);  % D�finition de la profondeur � la taille courante de la pile    
            stacksize=stacksize-1;          % R�duction de la taille de la pile de 1 - r�hauss� si branchment, laiss� telle quel si dominance. 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if(verbose)
                fprintf("\n---\nIt�ration n�%d\n",c);
                fprintf("   |S| = %d  \n",stacksize+1);
                fprintf("   Depth : %d\n",depth);
                fprintf("   Stack = ");fprintf(repmat('%d ', 1, length(stackdepth)), stackdepth);fprintf("\n")
                fprintf("   xLb = ");fprintf(repmat('%d ', 1, length(xLb(xstatus==1))), xLb(xstatus==1));fprintf("\n")
                fprintf("   xUb = ");fprintf(repmat('%d ', 1, length(xUb(xstatus==1))), xUb(xstatus==1));fprintf("\n")
            end;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       % STEP 2.2 : R�solution de la relaxation (CF MOd�le Ramzi et notes papier)
            [x,z,exitflag,output] = cplexqp(H'*H,-y'*H,Aineq,bineq,Aeq,Beq,xLb,xUb,x0);
            if(verbose)
                fprintf("> SOLVE : Statut : %s | z = %.5f \n",output.cplexstatusstring,z);  
                fprintf("> SYNTH : It�ration n�%d : z_inc = %.5f & z = %.5f\n",c,z_incumbent,z);
            end

            
            
       % STEP 2.3 : Dominance (Test de) - Bound
            % V est un vecteur contenant les indices des variables actives
            % et pour lequelles les bornes sup et inf sont diff�rentes
            V = find(xstatus==1 & xLb~=xUb);
            separation = 1;     % Noeud candidat � la s�paration (Par d�faut, au d�but, tout noeud est candidat � la s�paration)
            
            %Affichage alternatif  (minimaliste cas non verbose)
            if(~verbose && all(abs(round(x(V))-x(V))<errx) && exitflag >0 && z < z_incumbent)
                fprintf("> It�r n�%d : z* = %.3f & z = %.3f || ( %s , %d , %d) \n",c,z_incumbent,z,num2str(exitflag),z >= z_incumbent, all(abs(round(x(V))-x(V))<errx));
            end
            
            
            % Probl�me en minimisation - R�gles de dominances : 
            if(verbose), fprintf("? BOUND : ( %s , %d , %d)\n",num2str(exitflag),z >= z_incumbent, all(abs(round(x(V))-x(V))<errx));end
            if (exitflag <= 0) 
                if(verbose),fprintf("> BOUND : Noeud sond� par non-r�alisabilit�.\n");end
                % Noeud sond� par non r�alisabilit� (the relax has no feasible solution)
                separation = 0;
            elseif (exitflag > 0 && z >= z_incumbent)
                % Noeud sond� par dominance (the relax has an optimal solution that is worst than the incumbent)
                if(verbose),fprintf("> BOUND : Noeud sond� par dominance  z_inc =  %.5f VS z =  %.5f \n",z_incumbent, z);end
                separation = 0;
            elseif (exitflag >0 && all(abs(round(x(V))-x(V))<errx))
                % Noeud sond� par optimalit� (the relax has an integer solution (here, a epsilon-good solution)-> Compare the optSol with the incumbent i.e. the best we known and update if necessary)
                if(verbose), fprintf("> BOUND : Noeud sond� par optimalit� z_inc =  %.5f VS z =  %.5f \n",z_incumbent, z);end
                separation = 0;
                z_incumbent = z;    % Valeur de la solution courante modifi�e si meilleure
                x_incumbent = x;    % Composition de la solution courante modifi�e si meilleure
                if(verbose), fprintf("--> z_inc =  %.5f \n",z_incumbent);end
            end
       % STEP 2.4 : Etape de branchement - Branch 
            % S�l�ction, si elle existe, d'une variable (candidate � la s�paration i.e. separation = 1) sur laquelle on va brancher (si elle existe)
            %disp(x(V));
            if (separation == 1 && ~isempty(V))
       % STEP 2.4.1 : Selection de la variable � brancher
                % Heuristique de choix de l'indice de la variable � brancher
                ixsep = Selecting_Variables(x ,y,H ,z ,V ,K,exitflag,heuristic,xUb,verbose);
                if(verbose),fprintf("> BRANCH : Indice de branchement : %d | Valeur de x(%d) = %.5f.\n",ixsep-ceil(lx/2),ixsep-ceil(lx/2),x(ixsep)); end
       % STEP 2.4.2 : Proc�dure de branchement          
                branch=1;
                domain=[xLb(ixsep) xUb(ixsep)];
                sepdepth=depth;    
                while branch==1
       % STEP 2.4.2.a : Calcul des domaines apr�s branchement sur la variable x(ixsep)
                    [domainA,domainB] = Selecting_Branch(x,ixsep,domain,heuristic.choice_Separation,verbose);
                    if(verbose),fprintf("? domA : [%.2f,%.2f] - domB : [%.2f,%.2f]\n", domainA(1),domainA(2),domainB(1),domainB(2)); end
                    sepdepth=sepdepth+1;            % Dans le cas d'un branchement x_i = 1, incr�ment de la profondeur pour l'it�ration suivante ie on dit � l'algorithme d'aller chercher le prochain noeud � �valuer en DFS (rmq, sepdepth n'est pas mis � jour dans le cas d'une dominance, il va d�croitre en m�me temps que la taille)                  
                    stacksize=stacksize+1;          % On met � jour la taille de la pile de telle sorte � ce qu'il puisse aller taper une it�raton plus profond�ment
                    if(verbose),fprintf("? Sepdepth = %d    ||    Stacksize = %d    ||    i = %d \n",sepdepth,stacksize, ixsep-ceil(lx/2));end
                    %%%%%%
                    stackxlb(:,stacksize)=xLb;                    
                    stackxlb(ixsep,stacksize)=domainB(1);   % Mets � jour la borne inf sur la valeur ixsep du vecteur des binf de x apr�s "branchement" avec la valeur inf du domaine de branchement B
                    stackxub(:,stacksize)=xUb;
                    stackxub(ixsep,stacksize)=domainB(2);   % Mets � jour la borne sup sur la valeur ixsep du vecteur des bsup de x apr�s "branchement" avec la valeur sup du domaine de branchement B
                    %%%%%%
                    if(verbose),fprintf("> UP Stack(avB) = ");fprintf(repmat('%d ', 1, length(stackdepth)), stackdepth);fprintf("\n");end
                    stackdepth(1,stacksize)=sepdepth;
                    if(verbose),fprintf("> UP Stack(apB) = Stack(avA) = ");fprintf(repmat('%d ', 1, length(stackdepth)), stackdepth);fprintf("\n");end
                    if domainA(1)==domainA(2)
                        stacksize=stacksize+1;
                        stackxlb(:,stacksize)=xLb;
                        stackxlb(ixsep,stacksize)=domainA(1);
                        stackxub(:,stacksize)=xUb;
                        stackxub(ixsep,stacksize)=domainA(2);
                        stackdepth(1,stacksize)=sepdepth;
                        if(verbose),fprintf("> UP Stack(apA) = ");fprintf(repmat('%d ', 1, length(stackdepth)), stackdepth);fprintf("\n");end
                        branch = 0;     %On stop l'op�ration de branchement pour ce noeud
                    else 
                        domain=domainA;
                        branch=1;
                        errMsgG='Probleme binaire: branch=1.'; return;
                    end
                end
            end
            % Mise � jour du chronom�tre 
            timerelaxation = timerelaxation + output.time;
            if(verbose),fprintf("\n\n");end
        end
        
        %t = toc;
    end    
    
    t=timerelaxation;
    Z = z_incumbent;
    X = x_incumbent;
    errMsgG='_';
    exitflag=1;
    


   disp(['*** Number of nodes    : ',num2str(c)]);
   disp(['*** Temps de resolution    : ',num2str(t)]);
   
end



function [] = print_current_info(c,z_inc,z,x)
    nbX0 = sum(x(:)==0);
    nbXinf = sum(x(:)==inf);
    nbXn0 = sum(x(:)~=0 & x(:) ~= inf);
    fprintf("> SYNTH : It�ration n�%d : z_inc = %.5f & z = %.5f\n",c,z_inc,z)
    %idxXn0 = find(x(:)~=0 & x(:) ~= inf);
    %idxX0 = find(x(:)==0);
    %fprintf("It�ration n�%d\n> z_inc = %.5f\n> z = %.5f\n> |x(i)==0| = %d   |x(i)!=0| = %d   |x(i)==oo| = %d\n",c,z_inc,z,nbX0,nbXn0,nbXinf)
    %fprintf("> idx(!=0)\n");
    %fprintf(repmat('%d ', 1, length(idxXn0)), idxXn0)
    %fprintf("\n> idx(==0)\n")
    %fprintf(repmat('%d ', 1, length(idxX0)), idxX0)
end

