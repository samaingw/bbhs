%{
input_Checking
    Input :
        - x0 : la solutio initiale (vide) � l'appel du BB
        - xstat (=xtype) : structure type de la solution
        - xl : bornes inf sur les x
        - xu : bornes sup sur les x
        - a : matrice des ctr d'ineq membre gauche
        - b : vecteur des ctr d'ineq membre droit
        - aeq=[] : matrice des ctr d'eq membre gauche
        - beq=[] : vecteur des ctr d'eq membre droit

    Output :
        - errMsg (String) retourne le type d'erreur rencontr�e
        - stopFlag (Bool�en) retourne vrai si l'ensemble des arguments
        pass�s en entr�e de la fonction sont conformes.
        - xstatus
        - xlb : lower bounds sur les valeur de x {-M for i=1:100, 0 for i=101:200 }
        - xub : upper bounds sur les valeur de x {+M for i=1:100, 1 for i=101:200 }
        - A : matrice des ctr d'ineq membre gauche sous forme sparse (utiliser full(A) pour retrouver la matrice non sparse)
        - B : vecteur des ctr d'ineq membre droit
        - Aeq = []
        - Beq = []
%}
function [errMsgIC,stopFlag,xstatus,xlb,xub,A,B,Aeq,Beq] = input_Checking(x0,xstat,xl,xu,a,b,aeq,beq,verbose)
    % Init des flags
    errMsgIC = "_";
    stopFlag = 0;
    xstatus = [];
    xlb = [];
    xub =[];
    A = []; Aeq=[];
    B = []; Beq=[];
    %%%%%%%%%%%%%%%%
    if (isempty(x0))
        errMsgIC='Case 1 : No x0 found.'; stopFlag = 1; return;
    elseif ~isnumeric(x0) || ~isreal(x0) || size(x0,2)>1
       errMsgIC='Case 2 : x0 must be a real column vector.'; stopFlag = 1; return;
    end
    xstatus=zeros(size(x0));
    %%%%%%%%%%%%%%%%
    if (~isempty(xstat))
       if (isnumeric(xstat) && isreal(xstat) && all(size(xstat)<=size(x0)))
            if (all(xstat==round(xstat) & all(0<=xstat & xstat<=2)))
                xstatus(1:size(xstat))=xstat;
            else
                errMsgIC='Case 3 : xstatus must consist of the integers 0,1 en 2.';stopFlag = 1; return;
            end
       else
            errMsgIC='Case 4 : xstatus must be a real column vector the same size as x0.';stopFlag = 1; return;
       end
    end
    %%%%%%%%%%%%%%%%
    xlb=zeros(size(x0));
    %xlb(find(xstatus==0))=-inf;    % utiliser l'indexation logique plut�t
    %que le find()
    xlb(xstatus==0) =-inf; 
    %%%%%%%%%%%%%%%%
    if  (~isempty(xl))
       if (isnumeric(xl) && isreal(xl) && all(size(xl)<=size(x0)))
          xlb(1:size(xl,1))=xl;
       else
           errMsgIC='Case 5 : xlb must be a real column vector the same size as x0.'; stopFlag = 1; return; 
       end
    end
    %%%%%%%%%%%%%%%%
    if (any(x0<xlb))
       errMsgIC='Case 6 : x0 must be in the range xlb <= x0.'; stopFlag =1;return;
    elseif (any(xstatus==1 & (~isfinite(xlb) | xlb~=round(xlb))))
       errMsgIC='Case 7 : xlb(i) must be an integer if x(i) is an integer variable.';stopFlag =1; return;
    end
    %%%%%%%%%%%%%%%%
    % xlb(find(xstatus==2))=x0(find(xstatus==2));% utiliser l'indexation logique plut�t
    %que le find()
    xlb(xstatus==2)=x0(xstatus==2);
    
    %%%%%%%%%%%%%%%%
    xub=ones(size(x0));
    %xub(find(xstatus==0))=inf; % utiliser l'indexation logique plut�t
    %que le find()
    xub(xstatus==0) = inf;
    %%%%%%%%%%%%%%%%
    if (~isempty(xu))
       if(isnumeric(xu) && isreal(xu) && all(size(xu)<=size(x0)))
          xub(1:size(xu,1))=xu;
       else
           errMsgIC='Case 8 : xub must be a real column vector the same size as x0.'; stopFlag = 1; return; 
       end
    end
    %%%%%%%%%%%%%%%%
    if (any(x0>xub))
        errMsgIC='Case 9 : x0 must be in the range x0 <=xub.';stopFlag = 1; return;
    elseif (any(xstatus==1 & (~isfinite(xub) | xub~=round(xub))))
       errMsgIC='Case 10 : xub(i) must be an integer if x(i) is an integer variabale.'; stopFlag =1; return;
    end
    %xub(find(xstatus==2))=x0(find(xstatus==2));% utiliser l'indexation logique plut�t
    %que le find()
    xub(xstatus ==2) =x0(xstatus==2);
    %%%%%%%%%%%%%%%%
    if (~isempty(a))
       if (isnumeric(a) && isreal(a) && size(a,2)==size(x0,1))
           A=a;
       else
           errMsgIC='Case 11 : Matrix A not correct.'; stopFlag = 1;return; 
       end
    end
    %%%%%%%%%%%%%%%%
    if (~isempty(b))
       if (isnumeric(b) && isreal(b) && all(size(b)==[size(A,1) 1]))
           B=b;
       else
           errMsgIC='Case 12 : Column vector B not correct.';stopFlag = 1; return; 
       end
    end
    %%%%%%%%%%%%%%%%
    if (isempty(B) && ~isempty(A))
        B=zeros(size(A,1),1);
    end
    %%%%%%%%%%%%%%%%
    if (~isempty(aeq))
       if (isnumeric(aeq) && isreal(aeq) && size(aeq,2)==size(x0,1)) 
           Aeq=aeq;
       else
           errMsgIC='Case 13 : Matrix Aeq not correct.'; stopFlag = 1;return; 
       end
    end
    %%%%%%%%%%%%%%%%
    if (~isempty(beq))
       if (isnumeric(beq) && isreal(beq) && all(size(beq)==[size(Aeq,1) 1]))
           Beq=beq;
       else
           errMsgIC='Case 14: Column vector Beq not correct.'; stopFlag = 1;return;
       end
    end
    %%%%%%%%%%%%%%%%
    if(verbose)
        fprintf("************************\ninput_Checking\n> errMsg = %s\n> stopFlag = %d \n************************\n",errMsgIC,stopFlag);
    end
end
