function [ domainA,domainB ] = Selecting_Branch(x,ixsep,domain,choice,verbose)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% input:
% _ixsep : la variable de x sur laquelle on va brancher 
% _domain : le domain de x(ixsep) ie un vec ligne tq domain(1) = b_inf(domain(x(i)),  domain(2) = b_sup(domain(x(i))
% _choice : heuristique de branchement (� fixer dans optiMIP_minL2_constL0
% - heuristic.choice_Separation = 1;)
% output:
% _domainA : le domaine sur lequel on va brancher en premier
% _domainB : 

    xboundary=(domain(1)+domain(2))/2;
    %fprintf("> BRANCHING PROC dom(A=%.2f,B=%.2f) -> xbound = %.2f floor(xbound) =%.2f\n      i = %d, v(i) = %.4f\n",domain(1),domain(2),xboundary,floor(xboundary),ixsep,x(ixsep));
    switch choice
        case 0
            %fprintf("      Strat�gie 0 : ");
            if x(ixsep)<xboundary
                %fprintf("      Case 1 => x(i)< xbound \n");
                domainA=[domain(1) floor(xboundary)];
                domainB=[floor(xboundary+1) domain(2)];
            else
                %fprintf("      Case 2 => x(i)>= xbound \n");
                domainA=[floor(xboundary+1) domain(2)];
                domainB=[domain(1) floor(xboundary)];
            end
            %fprintf("            domA : [%.5f,%.5f]\n            domB : [%.5f,%.5f]\n", domainA(1),domainA(2),domainB(1),domainB(2)) 
        case 1
            %fprintf("Strat�gie 1 : ");
            if x(ixsep)==0
                %fprintf("      Case 1 => x(i)==0 \n");
                domainA=[domain(1) floor(xboundary)];
                domainB=[floor(xboundary+1) domain(2)];
            else
                %fprintf("      Case 2 => x(i)!= 0 \n");
                domainA=[floor(xboundary+1) domain(2)];
                domainB=[domain(1) floor(xboundary)];
            end     
            %fprintf("            domA : [%.5f,%.5f]\n            domB : [%.5f,%.5f]\n", domainA(1),domainA(2),domainB(1),domainB(2)) 
    end
    %fprintf("domA : [%.5f,%.5f]\ndomB : [%.5f,%.5f]\n", domainA(1),domainA(2),domainB(1),domainB(2)) 
end

