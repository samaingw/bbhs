function [ ixsep ] = Selecting_Variables(x, y, H, z, intVar, k, exitflag,heuristic,xub,verbose)
% HEURISTICS FOR SELECTION VARIABLE FROM THE INTEGER VALUES OF x
% heuristic.choice :  
%           _0_ default: THAT AFTER round(x_i) MAXIMIZE MORE z 
%           _1_ max(b_i)
%           _2_ min(b_i)
%           _3_ PEAKS_PURSUIT + max(bi)
%           _4_ OMP + max(bi)
%           _5_ MININFEAS
%           _6_ MAXINFEAS
%           _7_ PseudoCost
% H : DICTIONARY
% intVar : VECTOR OF POSITION OF INTEGER VALUES OF x
% z : OBJECTIF VALUE
% intVar = Le tableau V des variables candidates au branchement : V = find(xstatus==1 & xlb~=xub);
    global varCost;     %TODO Une id�e du pseudo cost.
    ordVarSel=heuristic.ordVarSel;
    choice= heuristic.choice_Selection;
    % Cas 0 : On a pass� un ordre dans la fonction dans l'attribut ie un tableau [] non vide dans optiMIP_minL2_constL0(y,H,K,x0,M,options,[ICI],verbose);
    % heuristic.ordVarSel (CF fonction main et fonction optiMIP_minL2_constLO
    Q=size(x,1)/2;
    n = length(ordVarSel);
    if(n>0)
        for i=1:n
            ixsep= find(intVar==ordVarSel(i)+Q);
            if(~isempty(ixsep)) 
                ixsep = ixsep(1);
                ixsep =intVar(ixsep);
                return
            end
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    % Switch Case : Voir commentaires de la fonction
    % Pr�ciser son choix de strat�gie de s�l�ction dans la fonction optiMIP_minL2_constLO (voir %Param�trage des �l�ments de r�solutionO)
    switch choice    
        case 1  % MAX Xi
            Q=size(x,1)/2;
            xi=x(intVar-Q);    
            x=zeros(2*Q,1);
            x(intVar-Q)=xi;
            ixsep =find(abs(x)== max(abs(xi)));
            ixsep = ixsep(1)+Q;
            %MAX bi
            %b=x(intVar);    
            %x=zeros(size(x,1),1);
            %x(intVar)=b;
            %[~,ixsep] = max(x);    % La fonction max retourne valeur et indice de position dans la liste
            %ixsep = ixsep(1);
        case 2 % MIN Xi
          b=x(intVar);    
          x=ones(size(x,1),1);
          x(intVar)=b;
          [~,ixsep] = min(x);
          ixsep = ixsep(1);
          
        case 3 %PP + MAX bi
          [peak locs]= findpeaks(abs(H*x),'NPEAKS',k,'SORTSTR','descend');   
          % A d�commenter si on veut �tudier les pics en d�tail.
          % plot(abs(H*x))
          % disp(peak); disp(locs); 
          locXi=locs(find(locs>10))-10;
          locBi=locXi+Q;
          i=0;j=0;
          while(i==0 && j<size(locBi,1))
           j=j+1;   
           i=find(intVar==locBi(j));
                if(size(i,1)==0 || x(intVar(i))==0)
                    i=0;
                end
          end
          if(i)
              ixsep=intVar(i);
          else
                b=x(intVar);    
                x=zeros(size(x,1),1);
                x(intVar)=b;
               [~,ixsep] = max(x);
                ixsep = ixsep(1); 
          end

        case 4 %OMP + max(bi) 
            H=H(:,1:Q);
            n=size(H);
            H1=zeros(n);
            R=y;
            i=1;
            d=0;
            while( isempty(find(intVar == d+Q)) && i<k+1 )
                [c,d] = max(abs(H'*R));
                H1(:,d)=H(:,d);
                H(:,d)=0;
                xo = H1 \ y;
                R = y - H1*xo;
                i=i+1;
            end
            if(~isempty(find(intVar == d+Q)))
                ixsep = d+Q;
                return
            end
            b=x(intVar);    
            x=zeros(size(x,1),1);
            x(intVar)=b;
            [~,ixsep] = max(x);
            ixsep = ixsep(1); 
        case 5 %plus proche de 0 ou 1
            b=x(intVar);    
            x=ones(size(x,1),1)*0.5;
            x(intVar)=b;
            [val_inf,ixsep_inf] = min(x);
            [val_sup,ixsep_sup] = max(x);
            if(abs(val_inf)<abs(1-val_sup))
                ixsep = ixsep_inf(1);
            else
                ixsep = ixsep_sup(1);
            end
        case 6 %plus proche de 0.5
            b=x(intVar);    
            x=ones(size(x,1),1);
            x(intVar)=b;
            [~,ixsep] = min(abs(x-0.5));
            ixsep = ixsep(1);
       case 7 % Pseudo cost
            %disp(varCost)
            b_pseudo= zeros(size(varCost));    
            b_pseudo(:,intVar)=1+varCost(:,intVar);  
            [~,ixsep] = max(b_pseudo(1,:)+b_pseudo(2,:)); 
            ixsep = ixsep(1);
            ixsep = 1;
    end       
end

