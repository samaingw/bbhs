close all
clear all
rng('default');
N = 10;
P = 10;
A = eye(P);
K = 4;

x = zeros(P,1);
permP = randperm(P); 
indNZ = permP(1:K);

x(indNZ) = rand(K,1);

y = A*x;
stem(y,'r','linewidth',2,'markersize',12);
hold on
sigma_bruit = 2;
y_bruit = A*x + sigma_bruit*randn(N,1);
stem(y_bruit,'d--','linewidth',2,'markersize',14);

