rng(1234567);
N = 16;         % Nombre d'observations
P = 15;         % Nombre de endmembers
H = randn(N,P); % Dictionaire
K = 4;          % Parcimonie

x = zeros(P,1);
permP = randperm(P); 
indNZ = permP(1:K);

x(indNZ) = rand(K,1);
x_Truth = x;

y = H*x;        % Sans bruit => unicit� du minimiseur

save("/Users/mehdilatif/Desktop/ETUDES/FAC/MASTER/M2/STAGE/Code/P_CC/multimedia_material_sectionV/miniInstance",'y','K','H','x_Truth')


